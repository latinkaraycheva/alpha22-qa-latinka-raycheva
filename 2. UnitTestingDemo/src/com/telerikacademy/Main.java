package com.telerikacademy;

import com.telerikacademy.users.User;

import java.util.ArrayList;

public class Main {
    
    public static void main(String[] args) {
        MyList<Integer> list = new MyArrayList<>(new Integer[]{1, 2, 3, 4, 5, 6});
        
        list.set(1, 20);
        
        System.out.println(list.get(1));
        
        for (int element : list) {
            System.out.print(element + " ");
        }

        System.out.println();
       list.add(8);
        System.out.println(list.getUsedPositions());

        
        System.out.println(list.findIndexOf(4));
        System.out.println(list.findIndexOf(7));

        
    }
    
}
