
import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class MyArrayListTests {
    private MyList<Integer> list;

    @BeforeEach
    public void init() {
        list = new MyArrayList<>();
    }


    @Test
    public void get_should_returnRightElement_when_indexIsValid() {
        //Arrange

        list.add(1);
        list.add(2);
        //Act
        Integer result = list.get(0);

        //Assert
        Assertions.assertEquals(1, result);

    }

    @Test
    public void get_should_throw_when_indexIsBelowZero() {
        //Arrange

        //Act //Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(-1));

    }

    @Test
    public void get_should_throw_whenIndexIsAboveTheUpperBound() {
        //Arrange
        //Act //Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(0));

    }

    @Test
    public void getLast_should_return_lastElement() {

        //Arrange
        list.add(1);
        list.add(4);

        Assertions.assertEquals(4, list.getLast());

    }

    @Test
    public void getFirst_should_return_firstElement() {
        list.add(1);
        list.add(2);
        Assertions.assertEquals(1, list.getFirst());
    }

    @Test
    public void getSize_should_return_arraySize() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(15);
        Assertions.assertEquals(4, list.getSize());
    }


    @Test
    public void add_should_return_addedElement() {
        list.add(16);
        Assertions.assertEquals(16, list.get(0));
    }

    @Test
    public void set_should_return_setElement() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(15);
        list.set(1, 20);
        Assertions.assertEquals(20, list.get(1));
    }
    @Test
    public void set_should_throw_whenIndexIsAboveTheUpperBound() {
        //Arrange
        //Act //Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.set(5,4));

    }

    @Test
    public void findIndexOf_should_return_correctIndex() {
        list.add(1);
        list.add(2);
        list.add(3);

        Assertions.assertEquals(2, list.findIndexOf(3));
    }

    @Test
    public void findIndexOf_should_return_minusOne() {
        list.add(1);
        list.add(2);
        list.add(3);

        Assertions.assertEquals(-1, list.findIndexOf(4));
    }

    @Test
    public void hasNext_should_returnFalse_when_DoNotHasNext() {

        Assertions.assertFalse(list.iterator().hasNext());
    }
    @Test
    public void hasNext_should_returnTrue_whenHasNext(){
        list.add(4);
        Assertions.assertTrue(list.iterator().hasNext());
    }


    @Test
    public void next_should_returnNext_when_hasNext() {
        list.add(3);
        list.add(5);
        Iterator<Integer> iterator = list.iterator();
        Assertions.assertEquals(3, iterator.next());
        Assertions.assertEquals(5, iterator.next());
    }

    @Test
    public void checkIndexIsValid_should_throw_when_indexIsNOtValid(){
        list.add(1);
        list.add(2);
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()->list.get(5));
    }


    //
//    @Test
//    public void test1() {
//        MyList<String> stringMyList = new MyArrayList<>();
//        Assertions.assertEquals(1, 2);
//    }
//
//    @Test
//    public void test2() {
//        MyList<Integer> List = new MyArrayList<>();
//        List.add(1);
//        List.add(2);
//        //Act
////        Integer result = List.get(2);
//
//        //Assert
//        Assertions.assertEquals(0, List.get(2));
//
//    }
//
    @Test
    public void checkDefaultSize() {
        MyList<Integer> list = new MyArrayList<>();
        Integer size = list.getSize();
        Assertions.assertEquals(4, size);
    }

    //
    @Test
    public void add_should_doubleArraysSize() {
        MyList<String> list = new MyArrayList<>(new String[]{"one", "two", "three", "four", "five"});
        list.add("six");
        Integer size = list.getSize();
        Assertions.assertEquals(10, size);
    }
}
