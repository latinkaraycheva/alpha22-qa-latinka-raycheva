package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {
    public final static int MIN_SEATS = 1;
    public final static int MAX_SEATS = 10;

    private final static String SEATS_FIELD = "Seats";
    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }
//
//    @Override
//    public String toString() {
//        return "CarImpl{" +
//                "seats=" + seats +
//                '}';
//    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %d", SEATS_FIELD, seats);
    }

    public void setSeats(int seats) {
        if (seats < MIN_SEATS || seats > MAX_SEATS) {
            throw new IllegalArgumentException("Seats must be between 1 and 10!");
        }
        this.seats = seats;
    }

    @Override
    public int getSeats() {
        return seats;
    }
    //look in DealershipFactoryImpl - use it to create proper constructor


}
