package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {
    public final static int MIN_CAPACITY = 1;
    public final static int MAX_CAPACITY = 100;

    private final static String WEIGHT_FIELD = "Weight Capacity";
    private int weightCapacity;


    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %dt", WEIGHT_FIELD, weightCapacity);
    }

//    @Override
//    public String toString() {
//        return "TruckImpl{" +
//                "weightCapacity=" + weightCapacity +
//                '}';
//    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    public void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < MIN_CAPACITY || weightCapacity > MAX_CAPACITY) {
            throw new IllegalArgumentException("Weight capacity must be between 1 and 100!");
        }
        this.weightCapacity = weightCapacity;
    }

    //look in DealershipFactoryImpl - use it to create proper constructor

}
