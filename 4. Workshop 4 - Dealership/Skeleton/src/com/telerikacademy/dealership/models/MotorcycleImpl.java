package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {
    public final static int MIN_CATEGORY_LENGTH = 3;
    public final static int MAX_CATEGORY_LENGTH = 10;

    private static final String CATEGORY_FIELD = "Category";
    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("%s: %s", CATEGORY_FIELD, category);
    }

    public void setCategory(String category) {
        if (category.length() < MIN_CATEGORY_LENGTH || category.length() > MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException("Category must be between 3 and 10 characters long!");
        }
        this.category = category;
    }
//
//    @Override
//    public String toString() {
//        return "MotorcycleImpl{" +
//                "category='" + category + '\'' +
//                '}';
//    }

    @Override
    public String getCategory() {
        return category;
    }

    //look in DealershipFactoryImpl - use it to create proper constructor
}
